import {AutomateState} from "./AutomateState";
import {GrammarOutput} from "./GrammarOutput";
import {AutomateTableInterface} from "./AutomateTableInterface";

export default interface AutomateResponse {
    isGrammarContextFree: boolean
    grammar: GrammarOutput
    first: Record<string, string[][]>
    follow: Record<string, string[][]>
    states: AutomateState[]
    numberOfStates: number
    table: AutomateTableInterface
}
