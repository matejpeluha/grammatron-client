
export interface GrammarInput {
    startSymbol: GrammarInputSymbol
    terminalSymbols: GrammarInputSymbol[]
    nonTerminalSymbols: GrammarInputSymbol[]
    rules: GrammarInputRule[]
    k: number
}

export interface GrammarInputRule {
    leftSide: GrammarInputSymbol
    rightSide: GrammarInputSymbol[]
}

export interface GrammarInputSymbol {
    text: string
    error: string
}
