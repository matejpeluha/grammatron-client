export interface GrammarOutput {
    k: number
    terminals: string[]
    nonTerminals: string[]
    rules: GrammarOutputRule[]
    startSymbol: string
}

export interface GrammarOutputRule {
    leftSide: string
    rightSide: string[]
}
