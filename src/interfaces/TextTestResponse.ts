import {GrammarOutputRule} from "./GrammarOutput";
import {TableStateItem} from "./AutomateTableInterface";

export interface GrammarTreeNode {
    name: string
    children: GrammarTreeNode[]
}

export interface TextTestResponse {
    result: boolean
    error?: string
    ruleStack?: GrammarOutputRule[]
    tree?: GrammarTreeNode|null
    analysisSteps?: AnalysisStep[]
}

export interface AnalysisStep {
    stateStack: number[]
    nextSymbols: string[]
    action: TableStateItem
}
