// states are rows, symbols are cells
import {GrammarOutputRule} from "./GrammarOutput";

export interface AutomateTableInterface {
    isCollisionPresent: boolean
    allTableSymbols: string[][]
    tableStates: AutomateTableStates
}

export type AutomateTableStates = TableStateItem[][]

export interface TableStateItem {
    symbols: string[]
    operation: 'reduce'|'shift'|'move'|'accept'
    gotoState?: number
    reduceRule?: GrammarOutputRule
}

export interface TableStateItemExtended extends TableStateItem {
    text: string
}
