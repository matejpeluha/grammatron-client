import {GrammarOutputRule} from "./GrammarOutput";

export interface AutomateState {
    items: AutomateStateItem[]
    isProcessed?: boolean
    goto: Record<string, number>
}

export interface AutomateStateItem {
    rule: GrammarOutputRule
    delimiterIndex: number
    expectedInput: string[][]
    isProcessed?: boolean
}
