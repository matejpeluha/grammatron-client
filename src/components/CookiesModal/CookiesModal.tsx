import Modal from "react-modal";
import MainButton from "../MainButton/MainButton";
import React from "react";

interface CookiesModalProps {
    isOpened: boolean
    onChangeCookiesSetting: (allowance: boolean) => void
}

const CookiesModal = ({isOpened, onChangeCookiesSetting}: CookiesModalProps) => {
    const modalStyles ={
        content: {
            backgroundColor: 'white',
            maxWidth: '800px',
            maxHeight: '350px',
            margin: 'auto',
            borderRadius: '12px'
        }
    }

    return (
        <Modal isOpen={isOpened}
               style={modalStyles}
               ariaHideApp={false}
               onRequestClose={() => onChangeCookiesSetting(false)}>
            <h1 className="text-center text-2xl md:text-4xl p-5">🍪 Povoliť Cookies 🍪</h1>
            <p className="text-center py-5">
                Týmto povoľujete používanie cookies na tejto stránke. Cookies sú využívané len na ukladanie a načítavanie gramatiky. Bez tohto súhlasu sa nedá pokračovať ďalej.
            </p>
            <div className="flex flex-row flex-wrap gap-10 w-full justify-center p-5">
                <MainButton text="Súhlasím" onClick={() => onChangeCookiesSetting(true)}/>
                <MainButton text="Nesúhlasím" onClick={() => onChangeCookiesSetting(false)} isWeak/>
            </div>
        </Modal>
    )
}

export default CookiesModal
