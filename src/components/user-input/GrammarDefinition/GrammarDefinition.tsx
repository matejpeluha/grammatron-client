import React, { FC } from 'react';
import SymbolList from "../symbols/SymbolList/SymbolList";
import StartSymbolInput from "../symbols/StartSymbolInput/StartSymbolInput";
import GrammarRulesList from "../rules/GrammarRulesList/GrammarRulesList";
import SizeKChooser from "../symbols/SizeKChooser/SizeKChooser";
import {GrammarInputRule, GrammarInputSymbol} from "../../../interfaces/GrammarInput";
import {useTranslation} from "react-i18next";

interface GrammarDefinitionProps {
    terminalSymbols: GrammarInputSymbol[]
    nonTerminalSymbols: GrammarInputSymbol[]
    startSymbol: GrammarInputSymbol
    rules: GrammarInputRule[],
    k: number,
    onChangeSymbols: (newSymbols: GrammarInputSymbol[], type?: string) => void
    onChangeStartSymbol: (uniqueSymbolText: GrammarInputSymbol) => void
    onChangeRules: (newRules: GrammarInputRule[]) => void
    onChangeK: (k: number) => void
}

const GrammarDefinition: FC<GrammarDefinitionProps> = (props: GrammarDefinitionProps) => {
    const {
        terminalSymbols,
        nonTerminalSymbols,
        startSymbol,
        rules,
        k,
        onChangeSymbols,
        onChangeStartSymbol,
        onChangeRules,
        onChangeK
    } = props

    const { t } = useTranslation()

    return (
        <div className="mx-2">
            <SizeKChooser k={k} onChange={onChangeK}/>
            <SymbolList header={t('grammar_definition.terminal_symbols')} type="terminal" allSymbols={terminalSymbols}
                        onChangeSymbols={onChangeSymbols}/>
            <SymbolList header={t('grammar_definition.nonterminal_symbols')} type="nonTerminal" allSymbols={nonTerminalSymbols}
                        onChangeSymbols={onChangeSymbols}/>
            <StartSymbolInput startSymbol={startSymbol} onChange={onChangeStartSymbol} nonTerminalSymbols={nonTerminalSymbols}/>
            <GrammarRulesList rules={rules} onChange={onChangeRules} terminalSymbols={terminalSymbols} nonTerminalSymbols={nonTerminalSymbols}/>
        </div>
    )
}

export default GrammarDefinition;
