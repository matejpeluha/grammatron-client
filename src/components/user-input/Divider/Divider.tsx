import React, { FC } from 'react';
import cx from "classnames";

interface DividerProps {
    className?: string
}

const Divider: FC<DividerProps> = ({ className }: DividerProps) => (
    <div className={cx("border-b-2 border-black", className)}/>
);

export default Divider;
