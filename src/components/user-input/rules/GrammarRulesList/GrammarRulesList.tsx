import React, { FC } from 'react';
import { FaPlusCircle } from "@react-icons/all-files/fa/FaPlusCircle"
import GrammarRuleRow from "../GrammarRuleRow/GrammarRuleRow";
import Divider from "../../Divider/Divider";
import {getEmptyRule} from "../../../../services/struct-creator";
import {GrammarInputRule, GrammarInputSymbol} from "../../../../interfaces/GrammarInput";
import {useTranslation} from "react-i18next";

interface GrammarRulesListProps {
    rules: GrammarInputRule[]
    terminalSymbols: GrammarInputSymbol[]
    nonTerminalSymbols: GrammarInputSymbol[]
    onChange: (newRules: GrammarInputRule[]) => void
}

const GrammarRulesList: FC<GrammarRulesListProps> = (props: GrammarRulesListProps) => {
    const {
        rules,
        terminalSymbols,
        nonTerminalSymbols,
        onChange } = props

    const { t } = useTranslation()

    const handleOnAddRule = () => {
        const emptyRule: GrammarInputRule = getEmptyRule()
        const newRules: GrammarInputRule[] = [...rules, emptyRule]
        onChange(newRules)
    }

    const handleOnDeleteRule = (ruleKey: number) => {
        let newRules = [...rules]
        if (newRules.length === 0) return
        newRules.splice(ruleKey, 1)
        if (newRules.length === 0) {
            newRules = [getEmptyRule()]
        }
        onChange(newRules)
    }

    const handleOnRuleChange = (changedRule: GrammarInputRule, ruleKey: number) => {
        const newRules = rules.map((oldRule, key) => key === ruleKey ? changedRule : oldRule)
        onChange(newRules)
    }

    return (
        <div className="m-2 flex flex-col">
            <h2 className="text-xl ml-2">{t('grammar_definition.rules')}</h2>
            {
                rules.map((rule, key) => {
                    return <GrammarRuleRow key={key} rule={rule} terminalSymbols={terminalSymbols} nonTerminalSymbols={nonTerminalSymbols}
                                           onChange={(changedRule: GrammarInputRule) => handleOnRuleChange(changedRule, key)}
                                           onDelete={() => handleOnDeleteRule(key)}/>
                })
            }
            <button className="w-48 my-4 ml-2 bg-blue-400 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded flex flex-row justify-center items-center gap-2"
                    onClick={handleOnAddRule}>
                <FaPlusCircle/>
                <span>{t('grammar_definition.add_rule')}</span>
            </button>
            <Divider />
        </div>
    )
}

export default GrammarRulesList;
