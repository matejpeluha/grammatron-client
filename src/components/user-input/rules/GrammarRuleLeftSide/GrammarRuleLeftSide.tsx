import React, { FC } from 'react';
import cx from "classnames";
import {GrammarInputSymbol} from "../../../../interfaces/GrammarInput";
import {useTranslation} from "react-i18next";

interface GrammarRuleLeftSideProps {
    symbol: GrammarInputSymbol
    onChange: (newSymbol: GrammarInputSymbol) => void
}

const GrammarRuleLeftSide: FC<GrammarRuleLeftSideProps> = ({ symbol, onChange }: GrammarRuleLeftSideProps) => {
    const { t } = useTranslation()

    const inputClassName = cx(
        "white border text-gray-900 text-sm rounded-lg block w-40 p-2.5 outline-none",
        {
            "border-gray-300": !symbol.error,
            "border-red-600": symbol.error
        }
    )

    const handleOnChange = (event: React.ChangeEvent) => {
        const target = event.target as HTMLSelectElement
        const newSymbol: GrammarInputSymbol = {text: target.value, error: ""}
        onChange(newSymbol)
    }

    return (
        <div className="max-w-[150px]">
            <input type="text" className={inputClassName} value={symbol?.text} onChange={handleOnChange}/>
            {
                <p className="text-red-600 max-w-[150px] break-words text-center sm:text-left pt-2 pl-1">{t(symbol.error)}</p>
            }
        </div>
    )
}

export default GrammarRuleLeftSide;
