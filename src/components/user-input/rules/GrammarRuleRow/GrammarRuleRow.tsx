import React, { FC } from 'react';
import GrammarRuleLeftSide from "../GrammarRuleLeftSide/GrammarRuleLeftSide";
import {FaArrowRight} from "@react-icons/all-files/fa/FaArrowRight";
import {validateRuleLeftSide} from "../../../../services/validator";
import {FaTrash} from "@react-icons/all-files/fa/FaTrash";
import SymbolList from "../../symbols/SymbolList/SymbolList";
import {GrammarInputRule, GrammarInputSymbol} from "../../../../interfaces/GrammarInput";
import cx from "classnames";

interface GrammarRuleRowProps {
    rule: GrammarInputRule
    terminalSymbols: GrammarInputSymbol[]
    nonTerminalSymbols: GrammarInputSymbol[]
    onChange: (rule: GrammarInputRule) => void
    onDelete: () => void
}

const GrammarRuleRow: FC<GrammarRuleRowProps> = (props: GrammarRuleRowProps) => {
    const {
        rule,
        terminalSymbols,
        nonTerminalSymbols,
        onChange,
        onDelete
    } = props

    const handleOnChangeLeftSide = (newLeftSide: GrammarInputSymbol) => {
        const validatedLeftSide: GrammarInputSymbol = validateRuleLeftSide(newLeftSide, nonTerminalSymbols)
        const newRule: GrammarInputRule = { leftSide: validatedLeftSide, rightSide: rule.rightSide }
        onChange(newRule)
    }

    const handleOnChangeRightSide = (newRightSide: GrammarInputSymbol[]) => {
        const newRule: GrammarInputRule = { leftSide: rule.leftSide, rightSide: newRightSide }
        onChange(newRule)
    }

    return (
        <div className="flex flex-row pr-2 gap-3 drop-shadow-mg rounded-lg bg-neutral-50 border border-gray-300 my-2 mx-4">
            <div className="flex flex-row gap-3">
                <div className="flex flex-col justify-center cursor-pointer border-r border-gray-300 hover:bg-slate-200 [&>svg]:hover:text-red-500 px-2 h-full" onClick={onDelete}>
                    <FaTrash className="text-xl text-gray-500 "/>
                </div>
            </div>
            <div className={cx("flex flex-col grow", "sm:flex-row")}>
                <div className={cx("flex flex-col pt-6 gap-3 justify-center items-center", "sm:py-6 sm:flex-row sm:justify-start sm:items-start")}>
                    <GrammarRuleLeftSide symbol={rule.leftSide} onChange={handleOnChangeLeftSide} />
                    <div className="h-10 flex mr-4 flex-col justify-center">
                        <FaArrowRight className={cx("rotate-90", "sm:rotate-0")}/>
                    </div>
                </div>
                <SymbolList allSymbols={rule.rightSide} onChangeSymbols={handleOnChangeRightSide} />
            </div>
        </div>
    )
}

export default GrammarRuleRow;
