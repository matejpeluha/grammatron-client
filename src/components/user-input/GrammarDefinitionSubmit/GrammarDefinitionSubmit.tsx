import React, {FC} from 'react';
import {isGrammarDefinitionBroken} from "../../../services/error-tester";
import {isStartSymbolUsedInRules} from "../../../services/validator";
import {analyseRequest} from "../../../services/api";
import AutomateResponse from "../../../interfaces/AutomateResponse";
import {GrammarInputRule, GrammarInputSymbol} from "../../../interfaces/GrammarInput";
import MainButton from "../../MainButton/MainButton";
import {useTranslation} from "react-i18next";

interface GrammarDefinitionSubmitProps {
    terminalSymbols: GrammarInputSymbol[]
    nonTerminalSymbols: GrammarInputSymbol[]
    startSymbol: GrammarInputSymbol
    rules: GrammarInputRule[]
    k: number
    onReceiveResponse: (response?: AutomateResponse) => void
}

const GrammarDefinitionSubmit: FC<GrammarDefinitionSubmitProps> = (props: GrammarDefinitionSubmitProps) => {
    const { terminalSymbols, nonTerminalSymbols, startSymbol, rules, k, onReceiveResponse } = props
    const { t } = useTranslation()
    const isGrammarBroken = isGrammarDefinitionBroken(startSymbol, terminalSymbols, nonTerminalSymbols, rules)
    const isStartSymbolUsed = isStartSymbolUsedInRules(startSymbol, rules)

    const handleOnClickButton = () => {
        if (!isGrammarBroken && isStartSymbolUsed) {
            analyseRequest({ startSymbol, terminalSymbols, nonTerminalSymbols, rules, k })
                .then(response => onReceiveResponse(response))
                .catch((e: Error) => {
                    onReceiveResponse()
                    console.log(e.message)
                })
        }
    }

    return (
        <div className="flex flex-col justify-center items-center w-full p-4">
            <MainButton text={t('grammar_definition.test_grammar')} isError={isGrammarBroken} onClick={handleOnClickButton}/>
            { isGrammarBroken && <p className="text-red-600 mt-4">{t('errors.grammar_with_errors')}</p> }
            { !isStartSymbolUsed && <p className="text-red-600 mt-2">{t('errors.start_symbol_not_used')}</p> }
        </div>
    )
}

export default GrammarDefinitionSubmit;
