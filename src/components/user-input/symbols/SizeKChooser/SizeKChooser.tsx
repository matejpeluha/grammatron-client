import Divider from "../../Divider/Divider";
import React from "react";

interface SizeKChooserProps {
    k: number
    onChange: (k: number) => void
}

const SizeKChooser = ({ k, onChange }: SizeKChooserProps) => {

    const handleOnChange = (event: React.ChangeEvent) => {
        event.preventDefault()
        const target = event.target as HTMLInputElement
        const newK = target.type === 'number' && Number(target.value) >= 1 ? Number(target.value) : 1
        onChange(newK)
    }

    return (
        <div className="m-2">
            <div className="flex flex-row items-center mb-4">
                <h2 className="text-xl ml-2">k=</h2>
                <input type="number" value={k} onChange={handleOnChange}
                       className="my-2 ml-4 bg-white border border-black text-gray-900 text-sm rounded-lg block w-40 p-2.5 outline-none"/>
            </div>
            <Divider />
        </div>
    )
}

export default SizeKChooser
