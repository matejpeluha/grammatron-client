import React, { FC } from 'react';
import cx from 'classnames'
import {validateStartSymbol} from "../../../../services/validator";
import Divider from "../../Divider/Divider";
import {GrammarInputSymbol} from "../../../../interfaces/GrammarInput";
import {useTranslation} from "react-i18next";

interface StartSymbolSelectProps {
    nonTerminalSymbols: GrammarInputSymbol[]
    startSymbol: GrammarInputSymbol
    onChange: (uniqueSymbolText: GrammarInputSymbol) => void
}

const StartSymbolInput: FC<StartSymbolSelectProps> = ({ nonTerminalSymbols, startSymbol, onChange }: StartSymbolSelectProps) => {
    const { t } = useTranslation()

    const inputClassName = cx(
        "my-2 ml-4 bg-white border text-gray-900 text-sm rounded-lg block w-40 p-2.5 outline-none",
        {
            "border-gray-300": !startSymbol.error,
            "border-red-600": startSymbol.error
        }
    )

    const handleOnChange = (event: React.ChangeEvent) => {
        const target = event.target as HTMLSelectElement
        const newStartSymbol: GrammarInputSymbol = {text: target.value.replace(/\s/g, ''), error: ""}
        const validatedStartSymbol = validateStartSymbol(newStartSymbol, nonTerminalSymbols)
        onChange(validatedStartSymbol)
    }

    return (
        <div className="m-2">
            <div className="mb-4">
                <h2 className="text-xl ml-2">{t('grammar_definition.start_symbol')}</h2>
                <input type="text" className={inputClassName} value={startSymbol?.text} onChange={handleOnChange}/>
                {
                    <span className="text-red-600 mb-2 ml-4">{t(startSymbol.error)}</span>
                }
            </div>
            <Divider />
        </div>
    )
}

export default StartSymbolInput;
