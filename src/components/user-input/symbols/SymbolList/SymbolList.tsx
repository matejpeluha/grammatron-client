import React, { FC } from 'react';
import { FaPlusCircle } from "@react-icons/all-files/fa/FaPlusCircle"
import SymbolInput from "../SymbolInput/SymbolInput";
import Divider from "../../Divider/Divider";
import {getEmptySymbol, getEpsilonSymbol} from "../../../../services/struct-creator";
import {AppConstants} from "../../../../constants";
import {GrammarInputSymbol} from "../../../../interfaces/GrammarInput";
import cx from "classnames";

interface SymbolListProps {
    header?: string | null
    type?: 'terminal'|'nonTerminal'
    allSymbols: GrammarInputSymbol[]
    // validateSymbol: (symbol: GrammarSymbol, terminalSymbols: GrammarSymbol[], nonTerminalSymbols: GrammarSymbol[]) => void
    onChangeSymbols: (newSymbols: GrammarInputSymbol[], type?: string) => void
}

const SymbolList: FC<SymbolListProps> = ({ header, type, allSymbols, onChangeSymbols }: SymbolListProps) => {
    const isEpsilonSymbolExisting = allSymbols.some(symbol => symbol.text === AppConstants.EPSILON)

    const handleOnAddSymbol = () => {
        const newSymbol: GrammarInputSymbol = getEmptySymbol()
        const newSymbols = [...allSymbols, newSymbol]
        onChangeSymbols(newSymbols, type)
    }

    const handleOnAddEpsilon = () => {
        const newSymbol: GrammarInputSymbol = getEpsilonSymbol()
        const newSymbols = !type ? [newSymbol] : [...allSymbols, newSymbol]
        onChangeSymbols(newSymbols, type)
    }

    const handleOnDeleteSymbol = (key: number) => {
        let newSymbols = [...allSymbols]
        if (newSymbols.length === 0) return
        newSymbols.splice(key, 1)
        if (newSymbols.length === 0) {
           newSymbols = [getEmptySymbol()]
        }
        onChangeSymbols(newSymbols, type)
    }

    const handleOnChangeSymbol = (key: number, text: string) => {
        let newSymbols
        if (text === AppConstants.EPSILON) {
            newSymbols = [getEpsilonSymbol()]
        } else {
            newSymbols = JSON.parse(JSON.stringify(allSymbols))
            if (newSymbols.length === 0) return
            newSymbols[key].text = text.replace(/\s/g, '')
        }
        onChangeSymbols(newSymbols, type)
    }

    return (
        <div className="m-2">
            { header && <h2 className="text-xl m-2">{header}</h2> }
            <div className={cx("flex flex-col flex-wrap gap-2 m-2 justify-center items-center", "sm:justify-start sm:items-start sm:flex-row")}>
                <div className={cx("flex flex-row flex-wrap gap-2 justify-center", "sm:justify-start")}>
                {
                    allSymbols.map((symbol, key) => {
                        return <SymbolInput key={key} symbol={symbol}
                                            onDelete={() => handleOnDeleteSymbol(key)}
                                            onChange={text => handleOnChangeSymbol(key, text)}/>
                    })
                }
                </div>
                <div className="flex flex-row">
                    {
                        ((!type && !isEpsilonSymbolExisting) || (type)) &&
                        <div className="flex flex-col justify-center h-10 m-2">
                            <FaPlusCircle className="text-3xl text-green-500 hover:text-green-700 cursor-pointer" onClick={handleOnAddSymbol}/>
                        </div>
                    }
                    {
                        !type && !isEpsilonSymbolExisting &&
                        <div className="flex flex-col justify-center h-10 m-2 ml-0" onClick={handleOnAddEpsilon}>
                            <div className="text-xl text-white bg-cyan-500 hover:bg-cyan-700 cursor-pointer rounded-full h-[29px] w-[29px] flex flex-row justify-center items-center">
                                {AppConstants.EPSILON}
                            </div>
                        </div>
                    }
                </div>
            </div>
            { header && <Divider /> }
        </div>
    )
}

export default SymbolList;
