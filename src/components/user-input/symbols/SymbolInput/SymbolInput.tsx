import React, { FC } from 'react';
import { FaTrash } from "@react-icons/all-files/fa/FaTrash"
import cx from "classnames"
import {GrammarInputSymbol} from "../../../../interfaces/GrammarInput";
import {useTranslation} from "react-i18next";

interface SymbolInputProps {
    symbol: GrammarInputSymbol
    onChange: (symbol: string) => void
    onDelete: () => void
}

const SymbolInput: FC<SymbolInputProps> = ({ symbol, onChange, onDelete }: SymbolInputProps) => {
    const { t } = useTranslation()

    const containerClassName = cx(
        "flex flex-row rounded-lg w-40 h-10 border",
        {
            "border-gray-300": !symbol.error,
            "border-red-600": symbol.error
        }
    )

    const handleOnChange = (event: React.ChangeEvent) => {
        event.preventDefault()
        const target = event.target as HTMLInputElement
        onChange(target.value)
    }

    return (
        <div className="m-2">
            <div className={containerClassName}>
                <input type="text" className="p-2 w-full h-full outline-none rounded-lg border-0" value={symbol.text} onChange={handleOnChange} />
                <div className="flex flex-col justify-center p-2 border-l text-gray-500 hover:text-red-500 cursor-pointer" onClick={onDelete}>
                    <FaTrash className="text-xl"/>
                </div>
            </div>
            {
                symbol && <span className="text-red-600">{t(symbol.error)}</span>
            }
        </div>
    )
}

export default SymbolInput;
