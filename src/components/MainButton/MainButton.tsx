import React from "react";
import cx from "classnames";

interface MainButtonProps {
    text: string
    isError?: boolean
    isWeak?: boolean
    className?: string
    onClick?: () => void
}

const MainButton = ({text, isError, isWeak, className, onClick}: MainButtonProps) => {
    const buttonClassName = cx(
        "p-3 rounded-lg border-2 w-56 h-16 cursor-pointer drop-shadow-xl transition-all",
        {
            "border-red-600 bg-red-100 text-red-600 hover:bg-red-200 hover:text-red-700": isError,
            "border-blue-600 bg-blue-400 text-white hover:bg-blue-600": !isError && !isWeak,
            "border-blue-400 bg-blue-100 text-blue-800 hover:bg-blue-400 hover:text-white": !isError && isWeak
        },
        className
    )
    return (
        <button className={buttonClassName} onClick={onClick}>
            {text}
        </button>
    )
}

export default MainButton
