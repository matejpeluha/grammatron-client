import MainButton from "../../MainButton/MainButton";
import {useTranslation} from "react-i18next";

interface SaveButtonsGroupProps {
    onSave: () => void
    onLoad: () => void
    onDelete: () => void
}

const SaveButtonsGroup = ({onSave, onLoad, onDelete}: SaveButtonsGroupProps) => {
    const { t } = useTranslation()

    return (
        <section className="flex flex-row flex-wrap gap-5 p-5 justify-center">
          <MainButton text={`🍪 ${t('save_buttons.save')}`} onClick={onSave}/>
          <MainButton text={`🍪 ${t('save_buttons.load')}`} onClick={onLoad}/>
          <MainButton text={t('save_buttons.delete')} onClick={onDelete}/>
        </section>
    )
}

export default SaveButtonsGroup
