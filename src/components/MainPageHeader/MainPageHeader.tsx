import React, {useEffect, useState} from "react";
import i18n from "i18next";

interface Language {
    short: string
    flag: string
}

const MainPageHeader = () => {
    const languages: Language[] = [
        {
            short: 'en',
            flag: '🇬🇧'
        },
        {
            short: 'sk',
            flag: '🇸🇰'
        }
    ]
    const getInitLanguage = (): Language => {
        const localStorageLanguage = localStorage.getItem(process.env.REACT_APP_LOCAL_STORAGE_LANGUAGE_KEY || '')
        const initLanguage = languages.find(language => language.short === localStorageLanguage)
        return initLanguage ?? languages[0]
    }

    const [currentLanguage, setCurrentLanguage] = useState<Language>(getInitLanguage)

    const handleChangeLanguage = (newLanguage: Language) => {
        setCurrentLanguage(newLanguage)
    }

    useEffect(() => {
        i18n.changeLanguage(currentLanguage.short).then(() => {
            localStorage.setItem(process.env.REACT_APP_LOCAL_STORAGE_LANGUAGE_KEY || '', currentLanguage.short)
        })
    }, [currentLanguage])

    return (
        <div>
            <div className="flex flex-row justify-end">
                {languages.map((language: Language, key: number) => (
                    <span key={key} className="text-3xl p-2 cursor-pointer" onClick={() => handleChangeLanguage(language)}>
                        { language.flag }
                    </span>
                ))}
            </div>
            <h1 className="text-center text-4xl sm:text-6xl md:text-7xl">Grammatron LR(k)</h1>
        </div>
    )
}

export default MainPageHeader
