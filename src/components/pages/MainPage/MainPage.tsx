import React, {FC, useEffect, useRef, useState} from 'react';
import GrammarDefinition from "../../user-input/GrammarDefinition/GrammarDefinition";
import {validateAllRules, validateAllSymbols, validateStartSymbol} from "../../../services/validator";
import {getEmptyRule, getEmptySymbol} from "../../../services/struct-creator";
import GrammarDefinitionSubmit from "../../user-input/GrammarDefinitionSubmit/GrammarDefinitionSubmit";
import Results from "../../user-output/Results/Results";
import AutomateResponse from "../../../interfaces/AutomateResponse";
import {GrammarInput, GrammarInputRule, GrammarInputSymbol} from "../../../interfaces/GrammarInput";
import SaveButtonsGroup from "../../saver/SaveButtonsGroup/SaveButtonsGroup";
import {useGrammarCookies} from "../../../services/cookies";
import CookiesModal from "../../CookiesModal/CookiesModal";
import MainPageHeader from "../../MainPageHeader/MainPageHeader";

interface MainPageProps {}

const MainPage: FC<MainPageProps> = () => {
    const [terminalSymbols, setTerminalSymbols] = useState<GrammarInputSymbol[]>([getEmptySymbol()])
    const [nonTerminalSymbols, setNonTerminalSymbols] = useState<GrammarInputSymbol[]>([getEmptySymbol()])
    const [startSymbol, setStartSymbol] = useState<GrammarInputSymbol>(getEmptySymbol())
    const [rules, setRules] = useState<GrammarInputRule[]>([getEmptyRule()])
    const [k, setK] = useState<number>(1)
    const [response, setResponse] = useState<AutomateResponse>()

    const {
        areCookiesAllowed,
        cookiesGrammar,
        saveGrammar,
        changeCookiesSettings,
        isCookiesModalOpened,
        openCookiesModal,
        lastCookieAction,
        setLastCookieAction
    } = useGrammarCookies()

    const resultRef = useRef<HTMLElement>(null)
    useEffect(() => {
        if (response && resultRef) {
            window.scrollTo(0, resultRef?.current?.offsetTop || window.screen.availHeight)
        }
    }, [response])

    useEffect(() => {
        const { newTerminalSymbols, newNonTerminalSymbols } = validateAllSymbols(terminalSymbols, nonTerminalSymbols)
        const newStartSymbol: GrammarInputSymbol = validateStartSymbol(startSymbol, newNonTerminalSymbols)
        const newRules: GrammarInputRule[] = validateAllRules(rules, newTerminalSymbols, newNonTerminalSymbols)

        if (JSON.stringify(newTerminalSymbols) !== JSON.stringify(terminalSymbols)) {
            setTerminalSymbols(newTerminalSymbols)
        }
        if (JSON.stringify(newNonTerminalSymbols) !== JSON.stringify(nonTerminalSymbols)) {
            setNonTerminalSymbols(newNonTerminalSymbols)
        }
        if (JSON.stringify(newStartSymbol) !== JSON.stringify(startSymbol)) {
            setStartSymbol(newStartSymbol)
        }
        if (JSON.stringify(newRules) !== JSON.stringify(rules)) {
            setRules(newRules)
        }
    }, [terminalSymbols, nonTerminalSymbols, startSymbol, rules])


    useEffect(() => {
        if (areCookiesAllowed) {
            if (lastCookieAction === 'load') {
                handleOnLoadCookies()
            } else if (lastCookieAction === 'save') {
                handleOnSaveCookies()
            }
        }
    }, [areCookiesAllowed])

    const handleOnChangeSymbols = (newSymbols: GrammarInputSymbol[], type?: string) => {
        if (type === "terminal") {
            setTerminalSymbols(newSymbols)
        } else if (type === "nonTerminal") {
            setNonTerminalSymbols(newSymbols)
        }
    }

    const handleOnSaveCookies = () => {
        setLastCookieAction('save')
        const newGrammar: GrammarInput = {
            terminalSymbols,
            nonTerminalSymbols,
            startSymbol,
            k,
            rules
        }
        saveGrammar(newGrammar)
    }

    const handleOnLoadCookies = () => {
        setLastCookieAction('load')
        if (cookiesGrammar && areCookiesAllowed) {
            setK(cookiesGrammar.k)
            setTerminalSymbols(cookiesGrammar.terminalSymbols)
            setNonTerminalSymbols(cookiesGrammar.nonTerminalSymbols)
            setStartSymbol(cookiesGrammar.startSymbol)
            setRules(cookiesGrammar.rules)
        } else if (!areCookiesAllowed){
            openCookiesModal()
        }
    }

    const handleOnDelete = () => {
        setK(1)
        setTerminalSymbols([getEmptySymbol()])
        setNonTerminalSymbols([getEmptySymbol()])
        setStartSymbol(getEmptySymbol())
        setRules([getEmptyRule()])
        setResponse(undefined)
    }


    return (
        <main className="pb-20">
            <MainPageHeader />
            <SaveButtonsGroup onSave={handleOnSaveCookies} onLoad={handleOnLoadCookies} onDelete={handleOnDelete}/>
            <GrammarDefinition terminalSymbols={terminalSymbols} nonTerminalSymbols={nonTerminalSymbols} k={k}
                               startSymbol={startSymbol} rules={rules} onChangeRules={setRules} onChangeK={setK}
                               onChangeSymbols={handleOnChangeSymbols} onChangeStartSymbol={setStartSymbol}/>
            <GrammarDefinitionSubmit terminalSymbols={terminalSymbols} nonTerminalSymbols={nonTerminalSymbols}
                                     startSymbol={startSymbol} rules={rules} k={k} onReceiveResponse={setResponse}/>
            <Results ref={resultRef} response={response}/>
            <CookiesModal isOpened={isCookiesModalOpened} onChangeCookiesSetting={changeCookiesSettings}/>
        </main>
    )
}

export default MainPage;
