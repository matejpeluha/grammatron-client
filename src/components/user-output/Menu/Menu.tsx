import React from "react";
import cx from 'classnames'
import {ResultCard} from "../Results/Results";

interface MenuProps {
    cardIndex: number
    resultCards: ResultCard[]
    onChangeCard: (cardIndex: number) => void
}

const Menu = ({cardIndex, resultCards, onChangeCard}: MenuProps) => {

    const getClassName = (index: number, isDisabled?: boolean) => {
        return cx(
            "transition-all w-28 h-12 border-2",
            {
                'bg-blue-600 text-white': index === cardIndex,
                'rounded-l-lg': index === 0,
                'rounded-r-lg': index === resultCards.length - 1,
                'bg-blue-50 text-gray-400': isDisabled,
                'hover:bg-blue-400 hover:text-white': !isDisabled
            })
    }

    return(
        <div className="flex flex-row justify-center p-10">
            <div className="flex flex-row">
                {resultCards.map((card: ResultCard, index: number) => (
                    <button key={index} onClick={() => onChangeCard(index)}
                            className={getClassName(index, card.isDisabled)}
                            disabled={card.isDisabled}>
                        {card.menuText}
                    </button>
                ))}
            </div>
        </div>
    )
}

export default Menu
