import SetsTable from "../SetsTable/SetsTable";

interface SetsTablesGroupProps {
    set: Record<string, string[][]>
    operationName: string
}

const SetsTablesGroup = ({ set, operationName }: SetsTablesGroupProps) => {
    return (
        <section className="flex flex-row flex-wrap gap-10 items-start justify-center">
            {Object.entries(set).map(([symbol, result]: [string, string[][]], key: number) => (
                <SetsTable key={key} symbol={symbol} result={result} operationName={operationName}/>
            ))}
        </section>
    )
}

export default SetsTablesGroup
