import ResultTable from "../../ResultTable/ResultTable";

interface SetsTableProps {
    symbol: string
    result: string[][]
    operationName: string
}

const SetsTable = ({symbol, result, operationName}: SetsTableProps) => {
    return (
        <ResultTable size='small' headerText={`${operationName.toUpperCase()}(${symbol})`}>
            {result.map((textString: string[], key: number) => (
                <div key={key} className="border-b text-center">
                    <p>
                        {textString.join(' ')}
                    </p>
                </div>
            ))}
        </ResultTable>
    )
}

export default SetsTable
