import {AnalysisStep} from "../../../../interfaces/TextTestResponse";
import ResultTable from "../../ResultTable/ResultTable";
import {useTranslation} from "react-i18next";
import AnalysisStepsTableRow from "../AnalysisStepsTableRow/AnalysisStepsTableRow";

interface AnalysisStepsTableProps {
    analysisSteps: AnalysisStep[]
}

const AnalysisStepsTable = ({ analysisSteps }: AnalysisStepsTableProps) => {
    const { t } = useTranslation()

    return (
        <ResultTable className="mt-10" size='full' headerText={t('results.analysis_table_title')}>
            <table className="w-full table-fixed">
                {analysisSteps.map((step: AnalysisStep, index: number) => (
                    <AnalysisStepsTableRow key={index} index={index} step={step} />
                ))}
            </table>
        </ResultTable>
    )
}

export default AnalysisStepsTable
