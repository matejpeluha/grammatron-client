import React from "react";
import MainButton from "../../../MainButton/MainButton";
import {useTranslation} from "react-i18next";
import AutomateResponse from "../../../../interfaces/AutomateResponse";
import {textTestRequest} from "../../../../services/api";
import Tree from 'react-d3-tree'
import {TextTestResponse} from "../../../../interfaces/TextTestResponse";
import cx from "classnames";
import AnalysisStepsTable from "../AnalysisStepsTable/AnalysisStepsTable";

interface TextTestProps {
    isDisabled?: boolean
    automateResponse?: AutomateResponse
    text?: string
    testResponse?: TextTestResponse
    onChangeText: (newText?: string) => void
    onChangeTestResponse: (newResponse?: any) => void
}

const TextTest = (props: TextTestProps) => {
    const { text, testResponse, isDisabled, automateResponse, onChangeText, onChangeTestResponse } = props
    const { t } = useTranslation()

    const handleOnChange = (event: React.ChangeEvent) => {
        event.preventDefault()
        const target = event.target as HTMLInputElement
        onChangeText(target.value)
    }

    const handleOnTest = () => {
        if (automateResponse?.table && text) {
            textTestRequest(automateResponse.grammar, automateResponse.table.tableStates, text.split(' '))
                .then(response => onChangeTestResponse(response))
                .catch((e: Error) => {
                    onChangeTestResponse(undefined)
                    console.log(e.message)
                })
        }
    }

    if (isDisabled) return null

    return(
        <section className="w-full flex flex-col justify-center items-center p-4">
            <textarea className="p-4 border w-full lg:w-8/12 resize-none h-40 drop-shadow-lg"
                      placeholder={t('results.write_text') || ''}
                      onChange={handleOnChange} value={text} />
            <MainButton text={t('results.text_test_button')}  className="m-10" isError={!text} onClick={handleOnTest}/>
            { testResponse && (
                <h3 className={cx("text-lg py-4", { "text-green-600": testResponse.result, "text-red-500": !testResponse.result })}>
                    { testResponse.result
                        ? t("results.text_test_success_result")
                        : testResponse.error
                            ? t(`results.${testResponse.error}`)
                            : t("results.text_test_failure_result")
                    }
                </h3>
            )}
            {testResponse && testResponse.tree && (
                <div className="border-2 border-black w-full h-[600px]">
                    <Tree data={testResponse.tree}
                          pathFunc='straight'
                          translate={{x: window.screen.width / 2, y: 50}}
                          orientation="vertical" />
                </div>
            )}
            {testResponse && testResponse.analysisSteps && (
                <AnalysisStepsTable analysisSteps={testResponse.analysisSteps} />
            )}
        </section>
    )
}

export default TextTest
