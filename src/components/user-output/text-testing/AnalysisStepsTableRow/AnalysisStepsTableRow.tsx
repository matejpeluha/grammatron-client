import {AnalysisStep} from "../../../../interfaces/TextTestResponse";

interface AnalysisStepsTableRowProps {
    step: AnalysisStep
    index: number
}

const AnalysisStepsTableRow = ({ step, index }: AnalysisStepsTableRowProps) => {
    return (
        <tr className="[&>td]:px-2 [&>td]:py-1 border-b">
            <td className="text-center w-16 border-r">
                {`${index}.`}
            </td>
            <td>
                <div className="flex flex-row flex-wrap gap-3 items-center h-full">
                    {step.stateStack.map((state: number, index: number) => (
                        <span key={index} >
                            {`S${state}`}
                        </span>
                    ))}
                </div>
            </td>
            <td className="text-right border-l">
                {step.nextSymbols.join(' ')}
            </td>
            <td className="border-l w-fit sm:w-62 md:w-72">
                <div className="flex flex-col justify-center items-center w-full">
                <b>
                    {step.action.operation === 'accept'
                        ? "ACC"
                        : step.action.operation === 'shift'
                            ? `SHIFT(${step.action.gotoState})`
                            : step.action.operation === 'reduce'
                                ? "REDUCE"
                                : ""
                    }
                </b>
                {step.action.operation === 'reduce' && (
                    <p>
                        {`${step.action.reduceRule?.leftSide} → ${step.action.reduceRule?.rightSide.join(' ')}`}
                    </p>
                )}
                </div>
            </td>
        </tr>
    )
}

export default AnalysisStepsTableRow
