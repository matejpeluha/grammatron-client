import React, {ForwardedRef, ForwardRefRenderFunction, MutableRefObject, useEffect, useState} from "react";
import Menu from "../Menu/Menu";
import SetsTablesGroup from "../sets/SetsTablesGroup/SetsTablesGroup";
import AutomateResponse from "../../../interfaces/AutomateResponse";
import StatesTablesGroup from "../states/StatesTablesGroup/StatesTablesGroup";
import {AppConstants} from "../../../constants";
import AutomateTable from "../automate/AutomateTable/AutomateTable";
import {useTranslation} from "react-i18next";
import TextTest from "../text-testing/TextTest/TextTest";
import {TextTestResponse} from "../../../interfaces/TextTestResponse";

export interface ResultCard {
    component: React.ReactNode
    menuText: string
    isDisabled?: boolean
}

const INIT_CARD_INDEX = 4

interface ResultsProps {
    response?: AutomateResponse
}

const Results: ForwardRefRenderFunction<HTMLElement, ResultsProps> = ((
    {response}: ResultsProps,
    ref: ForwardedRef<HTMLElement>
) => {
    const { t } = useTranslation()
    const [cardIndex, setCardIndex] = useState<number>(INIT_CARD_INDEX)
    const [text, setText] = useState<string>()
    const [testResponse, setTestResponse] = useState<TextTestResponse>()

    useEffect(() => {
        setText(undefined)
        setTestResponse(undefined)
        setCardIndex(INIT_CARD_INDEX)
    }, [response])

    if (!response) return null
    const firstFiltered = Object.fromEntries(
        Object.entries(response.first).filter(([key]) => {
            return !response.grammar.terminals.includes(key) && key !== AppConstants.EPSILON
        })
    )

    const resultCards: ResultCard[]  = [
        { menuText: 'JSON', component: <pre>{JSON.stringify(response, null, 3)}</pre> },
        { menuText: 'FIRST', component: <SetsTablesGroup operationName="FIRST" set={firstFiltered} /> },
        { menuText: 'FOLLOW', component: <SetsTablesGroup operationName="FOLLOW" set={response.follow} /> },
        { menuText: t('results.states'), component: <StatesTablesGroup states={response.states} /> },
        { menuText: t('results.automate'), component: <AutomateTable table={response.table}/> },
        {
            menuText: 'TEXT',
            isDisabled: response.table.isCollisionPresent,
            component: <TextTest isDisabled={response.table.isCollisionPresent}
                                 automateResponse={response}
                                 text={text} testResponse={testResponse}
                                 onChangeText={setText}
                                 onChangeTestResponse={setTestResponse}/>
        }
    ]

    return (
        <section ref={ref} className="flex flex-col justify-center items-center" >
            <Menu cardIndex={cardIndex} resultCards={resultCards} onChangeCard={setCardIndex}/>
            { resultCards[cardIndex].component }
        </section>
    )
})

export default React.forwardRef(Results)
