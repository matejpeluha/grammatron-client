import {AutomateState, AutomateStateItem} from "../../../../interfaces/AutomateState";
import ResultTable from "../../ResultTable/ResultTable";
import StateTableRow from "../StateTableRow/StateTableRow";
import {useTranslation} from "react-i18next";

interface StateTableProps {
    index: number
    state: AutomateState
}

const StateTable = ({index, state}: StateTableProps) => {
    const { t } = useTranslation()
    return (
        <ResultTable size='big' headerText={`${t('results.state')} ${index}`}>
             {state.items.map((item: AutomateStateItem, key: number) => (
                <StateTableRow key={key} item={item} goto={state.goto} isFirst={key === 0} />
             ))}
         </ResultTable>
  )
}

export default StateTable
