import {AutomateState} from "../../../../interfaces/AutomateState";
import StateTable from "../StateTable/StateTable";

interface StatesTablesGroupProps {
    states: AutomateState[]
}

const StateTablesGroups = ({states}: StatesTablesGroupProps) => {
  return (
      <div className="flex flex-row flex-wrap gap-10 items-start justify-center">
          {states.map((state: AutomateState, key: number) => <StateTable key={key} index={key} state={state}/>)}
      </div>
  )
}

export default StateTablesGroups
