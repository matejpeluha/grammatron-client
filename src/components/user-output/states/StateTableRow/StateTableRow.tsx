import {AutomateStateItem} from "../../../../interfaces/AutomateState";
import cx from "classnames";
import {useTranslation} from "react-i18next";

interface StateTableRowProps {
    item: AutomateStateItem
    goto: Record<string, number>
    isFirst?: boolean
}

const StateTableRow = ({item, goto, isFirst}: StateTableRowProps) => {
    const { t } = useTranslation()
    const ruleRightSideWithBullet = [...item.rule.rightSide]
    ruleRightSideWithBullet.splice(item.delimiterIndex, 0, '⚫')
    const rule = `${item.rule.leftSide} → ${ruleRightSideWithBullet.join(' ')}`

    let expectedInput = '[ '
    item.expectedInput.forEach((input: string[], index: number) => {
        expectedInput += input.join(' ')
        if (index < item.expectedInput.length - 1) {
            expectedInput += ', '
        } else {
            expectedInput += ' ]'
        }
    })

    const gotoSymbol = item.rule.rightSide[item.delimiterIndex]

    return (
        <div className={cx("border-b flex flex-row items-center", {"bg-blue-100": isFirst})}>
            <p className={cx("px-3 py-1 grow", {"font-bold": isFirst})}>
                {rule}
            </p>
            <p className="px-3 py-1 border-l grow">
                {expectedInput}
            </p>
            {
                gotoSymbol && (
                    <div className="px-3 py-1 border-l flex flex-row gap-1 grow">
                        <b>{gotoSymbol}</b>
                        <span>goto</span>
                        <b>{`${t('results.state')} ${goto[gotoSymbol]}`}</b>
                    </div>
                )
            }
        </div>
    )
}

export default StateTableRow
