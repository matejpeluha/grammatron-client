import {AutomateTableStates} from "../../../../interfaces/AutomateTableInterface";
import AutomateTableHeadCell from "../AutomateTableHeadCell/AutomateTableHeadCell";

interface AutomateTableHeadProps {
    tableStates: AutomateTableStates
}

const AutomateTableHead = ({tableStates}: AutomateTableHeadProps) => {
    return (
        <thead>
        <tr>
            <AutomateTableHeadCell isFirst/>
            {tableStates.map((_, index) => (
                <AutomateTableHeadCell key={index} index={index} isLast={index === tableStates.length - 1} />
            ))}
        </tr>
        </thead>
    )
}

export default AutomateTableHead
