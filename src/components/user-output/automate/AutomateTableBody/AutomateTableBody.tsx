import {AutomateTableStates} from "../../../../interfaces/AutomateTableInterface";
import AutomateTableBodyRow from "../AutomateTableBodyRow/AutomateTableBodyRow";

interface AutomateTableBodyProps {
    tableStates: AutomateTableStates
    allTableSymbols: string[][]
}

const AutomateTableBody = ({tableStates, allTableSymbols}: AutomateTableBodyProps) => {
    return (
        <tbody>
        {allTableSymbols.map((symbols: string[], key: number) => (
            <AutomateTableBodyRow key={key} symbols={symbols} tableStates={tableStates}/>
        ))}
        </tbody>
    )
}

export default AutomateTableBody
