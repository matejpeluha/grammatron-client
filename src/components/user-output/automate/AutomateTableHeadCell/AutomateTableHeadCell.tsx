import cx from "classnames";

interface AutomateTableHeadCellProps {
    index?: number
    text?: string
    isFirst?: boolean
    isLast?: boolean
}

const AutomateTableHeadCell = ({index, text, isFirst, isLast}: AutomateTableHeadCellProps) => {
    const className = cx(
        "py-3 px-5 bg-blue-100 border-none",
        {
            "rounded-tl-xl": isFirst,
            "rounded-tr-xl": isLast,
        }
    )

    return (
        <th className={className}>
            {
                text || (index != null ? `S${index}` : '')
            }
        </th>
    )
}

export default AutomateTableHeadCell
