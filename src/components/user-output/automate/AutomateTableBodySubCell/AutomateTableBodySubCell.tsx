import {TableStateItemExtended} from "../../../../interfaces/AutomateTableInterface";
import Divider from "../../../user-input/Divider/Divider";

interface AutomateTableBodySubCellProps {
    item: TableStateItemExtended
    isLast?: boolean
}

const AutomateTableBodySubCell = ({ item, isLast }: AutomateTableBodySubCellProps) => {
    return (
        <>
            <div className="w-full text-center flex flex-col">
                <span className="text-md">{item.text}</span>
                {item?.reduceRule && (
                    <span className="text-sm">
                        {`${item.reduceRule.leftSide} → ${item.reduceRule.rightSide.join(' ')}`}
                    </span>
                )}
            </div>
            { !isLast && <Divider className="w-full border-gray-400"/> }
        </>
    )
}

export default AutomateTableBodySubCell
