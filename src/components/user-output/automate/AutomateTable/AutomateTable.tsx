import {AutomateTableInterface} from "../../../../interfaces/AutomateTableInterface";
import AutomateTableHead from "../AutomateTableHead/AutomateTableHead";
import AutomateTableBody from "../AutomateTableBody/AutomateTableBody";
import {useTranslation} from "react-i18next";
import cx from "classnames";

interface AutomateProps {
    table: AutomateTableInterface
}

const AutomateTable = ({ table }: AutomateProps) => {
    const { tableStates, allTableSymbols, isCollisionPresent } = table
    const { t } = useTranslation()

    return (
        <section className="m-auto px-5 w-full">
            <h1 className={cx("text-center text-lg", { "text-red-500": isCollisionPresent, "text-green-600": !isCollisionPresent})}>
                {t(`results.collision_${isCollisionPresent}`)}
            </h1>
            <div className="overflow-x-auto mx-2 mt-2">
                <table className="border-separate border-spacing-0 drop-shadow-2xl bg-white m-auto">
                    <AutomateTableHead tableStates={tableStates} />
                    <AutomateTableBody tableStates={tableStates} allTableSymbols={allTableSymbols}/>
                </table>
            </div>
        </section>
    )
}

export default AutomateTable
