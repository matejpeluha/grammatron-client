import {AutomateTableStates, TableStateItem} from "../../../../interfaces/AutomateTableInterface";
import AutomateTableBodyCell from "../AutomateTableBodyCell/AutomateTableBodyCell";
import AutomateTableHeadCell from "../AutomateTableHeadCell/AutomateTableHeadCell";

interface AutomateTableBodyRowProps {
    symbols: string[]
    tableStates: AutomateTableStates
}

const AutomateTableBodyRow = ({symbols, tableStates}: AutomateTableBodyRowProps) => {
    return (
        <tr>
            <AutomateTableHeadCell text={symbols.join(' ')}/>
            {tableStates.map((tableState: TableStateItem[], index: number) => (
                <AutomateTableBodyCell key={index} statesItems={tableState} symbols={symbols}/>
            ))}
        </tr>
    )
}

export default AutomateTableBodyRow
