import {TableStateItem, TableStateItemExtended} from "../../../../interfaces/AutomateTableInterface";
import AutomateTableBodySubCell from "../AutomateTableBodySubCell/AutomateTableBodySubCell";
import cx from "classnames";

interface AutomateTableBodyCellProps {
    statesItems: TableStateItem[]
    symbols: string[]
}

const AutomateTableBodyCell = ({statesItems, symbols}: AutomateTableBodyCellProps) => {
    const items: TableStateItem[]|undefined = statesItems.filter(item => {
        return JSON.stringify(symbols) === JSON.stringify(item.symbols)
    })

    const extendedItems: TableStateItemExtended[] = items.map((item: TableStateItem) => {
        const text = item
            ? item.operation === 'accept'
                ? 'ACC'
                : item.operation === 'move'
                    ? item.gotoState
                    : item.operation === 'shift'
                        ? `SHIFT(${item.gotoState})`
                        : item.operation === 'reduce'
                            ? 'REDUCE'
                            : ''
            : ''
        return { ...item, text } as TableStateItemExtended
    })

    const isCollision = extendedItems.length > 1

    return (
        <td className={cx("border", {"border-red-500": isCollision})}>
            <div className={cx("flex flex-col justify-center items-center p-2 gap-2", {"bg-red-100": isCollision})}>
                {extendedItems.map((item: TableStateItemExtended, index: number) => (
                    <AutomateTableBodySubCell key={index} item={item} isLast={index === extendedItems.length - 1}/>
                ))}
            </div>
        </td>
    )
}

export default AutomateTableBodyCell
