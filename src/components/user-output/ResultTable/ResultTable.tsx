import {ReactNode} from "react";
import cx from 'classnames'

interface ResultTableProps {
    headerText: string
    children: ReactNode
    size: 'big'|'small'|'full'
    className?: string
}

const ResultTable = ({headerText, size, children, className}: ResultTableProps) => {
  return (
      <div className={
          cx("drop-shadow-xl rounded-lg border-2",
              {'w-64': size === 'small', 'w-96': size === 'big', 'w-full': size === 'full'},
              className)
      }>
          <div className="w-full border-b-2 bg-blue-600 text-white p-2 rounded-t-lg">
              <h2 className="text-center">{headerText}</h2>
          </div>
          <div className="rounded-b-lg bg-white">
              {children}
          </div>
      </div>
  )
}

export default ResultTable
