import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import './i18n'

import MainPage from "./components/pages/MainPage/MainPage";


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
)


root.render(
  <React.StrictMode>
    <MainPage />
  </React.StrictMode>
)

