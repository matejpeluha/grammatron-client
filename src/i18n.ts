import i18n from "i18next";
import {initReactI18next} from "react-i18next";
import en_translation from './translations/en.json';
import sk_translation from './translations/sk.json';

export const getInit = () => {
    return {
        en: {
            translation: en_translation
        },
        sk: {
            translation: sk_translation
        }
    }
}

const storedLanguage = localStorage.getItem(process.env.REACT_APP_LOCAL_STORAGE_LANGUAGE_KEY || '')
const language = storedLanguage && ['en', 'sk'].includes(storedLanguage) ? storedLanguage : 'en';
if (!storedLanguage) {
    localStorage.setItem(process.env.REACT_APP_LOCAL_STORAGE_LANGUAGE_KEY || '', language)
}

i18n.use(initReactI18next)
    .init({
        resources: getInit(),
        lng: language,
        fallbackLng: "en"
    });
