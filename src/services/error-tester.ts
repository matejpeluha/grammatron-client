import {GrammarInput, GrammarInputRule, GrammarInputSymbol} from "../interfaces/GrammarInput";

export const isSymbolBroken = (symbol: GrammarInputSymbol): boolean => {
    return !!symbol.error
}

export const areSymbolsBroken = (allSymbols: GrammarInputSymbol[]): boolean => {
    return !!(allSymbols.find(isSymbolBroken))
}

export const isRuleBroken = (rule: GrammarInputRule): boolean => {
    return isSymbolBroken(rule.leftSide) || areSymbolsBroken(rule.rightSide)
}

export const areRulesBroken = (allRules: GrammarInputRule[]): boolean => {
    return !!(allRules.find(isRuleBroken))
}

export const isGrammarDefinitionBroken = (startSymbol: GrammarInputSymbol, terminalSymbols: GrammarInputSymbol[], nonTerminalSymbols: GrammarInputSymbol[], rules: GrammarInputRule[]) => {
    return isSymbolBroken(startSymbol)
        || areSymbolsBroken(terminalSymbols)
        || areSymbolsBroken(nonTerminalSymbols)
        || areRulesBroken(rules)
}
