import {AppConstants} from "../constants";
import {GrammarInputRule, GrammarInputSymbol} from "../interfaces/GrammarInput";

export const validateAllSymbols = (terminalSymbols: GrammarInputSymbol[], nonTerminalSymbols: GrammarInputSymbol[]) => {
    const newTerminalSymbols: GrammarInputSymbol[] = JSON.parse(JSON.stringify(terminalSymbols))
    const newNonTerminalSymbols: GrammarInputSymbol[] = JSON.parse(JSON.stringify(nonTerminalSymbols))
    newTerminalSymbols.forEach(symbol => {
        validateTerminalSymbol(symbol, terminalSymbols, nonTerminalSymbols)
    })
    newNonTerminalSymbols.forEach(symbol => {
        validateNonTerminalSymbol(symbol, terminalSymbols, nonTerminalSymbols)
    })
    return { newTerminalSymbols, newNonTerminalSymbols }
}

export const validateTerminalSymbol = (symbol: GrammarInputSymbol, terminalSymbols: GrammarInputSymbol[], nonTerminalSymbols: GrammarInputSymbol[]) => {
    if (symbol.text === "") {
        symbol.error =  "errors.missing_symbol"
    } else if (symbol.text === AppConstants.EPSILON) {
        symbol.error = 'errors.symbol_can_not_be_epsilon'
    } else if (terminalSymbols.filter(s => s.text === symbol.text).length > 1){
        symbol.error = 'errors.terminal_exists'
    } else if (nonTerminalSymbols.find(s => s.text === symbol.text)) {
        symbol.error = 'errors.used_in_nonterminals'
    } else {
        symbol.error = ""
    }
}

export const validateNonTerminalSymbol = (symbol: GrammarInputSymbol, terminalSymbols: GrammarInputSymbol[], nonTerminalSymbols: GrammarInputSymbol[]) => {
    if (symbol.text === "") {
        symbol.error =  'errors.missing_symbol'
    } else if (symbol.text === AppConstants.EPSILON) {
        symbol.error = 'errors.symbol_can_not_be_epsilon'
    } else if (nonTerminalSymbols.filter(s => s.text === symbol.text).length > 1){
        symbol.error = 'errors.terminal_exists'
    } else if (terminalSymbols.find(s => s.text === symbol.text)) {
        symbol.error = 'errors.used_in_terminals'
    } else {
        symbol.error = ""
    }
}

export const validateStartSymbol = (symbol: GrammarInputSymbol, nonTerminalSymbols: GrammarInputSymbol[]) => {
    const newStartSymbol: GrammarInputSymbol = { ...symbol }
    if (newStartSymbol.text === "") {
        newStartSymbol.error =  'errors.missing_start_symbol'
    } else if (!nonTerminalSymbols.find(s => s.text === newStartSymbol.text)){
        newStartSymbol.error = 'errors.start_symbol_not_in_nonterminals'
    } else if (nonTerminalSymbols.find(s => s.text === newStartSymbol.text)?.error) {
        newStartSymbol.error = 'errors.wrong_symbol'
    } else {
        newStartSymbol.error = ""
    }
    return newStartSymbol
}

export const validateAllRules = (rules: GrammarInputRule[], terminalSymbols: GrammarInputSymbol[], nonTerminalSymbols: GrammarInputSymbol[]) => {
    return rules.map(rule => {
        const newLeftSide = validateRuleLeftSide(rule.leftSide, nonTerminalSymbols)
        const newRightSide = validateRuleRightSide(rule.rightSide, terminalSymbols, nonTerminalSymbols)
        return { leftSide: newLeftSide, rightSide: newRightSide }
    })
}

export const validateRuleLeftSide = (symbol: GrammarInputSymbol, nonTerminalSymbols: GrammarInputSymbol[]) => {
    const newStartSymbol: GrammarInputSymbol = { ...symbol }
    if (newStartSymbol.text === "") {
        newStartSymbol.error =  'errors.missing_symbol'
        return newStartSymbol
    }
    const sameNonTerminalSymbol = nonTerminalSymbols.find(s => s.text === newStartSymbol.text)
    if (!sameNonTerminalSymbol){
        newStartSymbol.error = 'errors.symbol_not_in_nonterminals'
    } else if (sameNonTerminalSymbol?.error) {
        newStartSymbol.error = 'errors.wrong_symbol'
    } else {
        newStartSymbol.error = ""
    }
    return newStartSymbol
}

export const validateRuleRightSide = (rightSide: GrammarInputSymbol[], terminalSymbols: GrammarInputSymbol[], nonTerminalSymbols: GrammarInputSymbol[]) => {
    return rightSide.map((symbol: GrammarInputSymbol) => {
        return validateRuleRightSideSymbol(symbol, terminalSymbols, nonTerminalSymbols)
    })
}

export const validateRuleRightSideSymbol = (symbol: GrammarInputSymbol, terminalSymbols: GrammarInputSymbol[], nonTerminalSymbols: GrammarInputSymbol[]) => {
    const newSymbol: GrammarInputSymbol = { ...symbol }
    if (newSymbol.text === "") {
        newSymbol.error =  'errors.missing_symbol'
        return newSymbol
    } else if (symbol.text === AppConstants.EPSILON) {
        newSymbol.error = ""
        return newSymbol
    }
    const sameTerminalSymbol = terminalSymbols.find(terminal => terminal.text === newSymbol.text)
    const sameNonTerminalSymbol = nonTerminalSymbols.find(nonTerminal => nonTerminal.text === newSymbol.text)
    if (!sameTerminalSymbol && !sameNonTerminalSymbol) {
        newSymbol.error = 'errors.non_existing_symbol'
    } else if (sameTerminalSymbol?.error || sameNonTerminalSymbol?.error) {
        newSymbol.error = 'errors.wrong_symbol'
    } else {
        newSymbol.error = ""
    }
    return newSymbol
}

export const isStartSymbolUsedInRules = (startSymbol: GrammarInputSymbol, rules: GrammarInputRule[]): boolean => {
    return !!(rules.find(rule => rule.leftSide.text === startSymbol.text))
}
