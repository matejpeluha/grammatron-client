import axios from 'axios'
import {GrammarInput, GrammarInputRule} from "../interfaces/GrammarInput";
import AutomateResponse from "../interfaces/AutomateResponse";
import {AutomateTableStates} from "../interfaces/AutomateTableInterface";
import {GrammarOutput} from "../interfaces/GrammarOutput";

export const textTestRequest = (grammar: GrammarOutput, tableStates: AutomateTableStates, text: string[]) => {
    const url = new URL(`${process.env.REACT_APP_API_ENDPOINT}/api/text-test`)
    const body = { grammar, tableStates, text }
    return axios.post(url.toString(), body).then(res => res.data)
}

export const analyseRequest = (grammar: GrammarInput): Promise<AutomateResponse> => {
    const url = new URL(`${process.env.REACT_APP_API_ENDPOINT}/api/analyse/grammar`)
    appendGrammarUrlParams(url, grammar)
    return axios.get(url.toString()).then(res => res.data).catch((e) => console.log(e))
}

const appendGrammarUrlParams = (url: URL, { startSymbol, terminalSymbols, nonTerminalSymbols, rules, k }: GrammarInput) => {
    url.searchParams.set('k', String(k))
    url.searchParams.set('startSymbol', startSymbol.text)
    terminalSymbols.forEach(symbol => {
        url.searchParams.append('terminals', symbol.text)
    })
    nonTerminalSymbols.forEach(symbol => {
        url.searchParams.append('nonTerminals', symbol.text)
    })
    rules.forEach((rule: GrammarInputRule, rulesCounter: number) => {
        url.searchParams.set(`rules[${rulesCounter}][leftSide]`, rule.leftSide.text)
        rule.rightSide.forEach(symbol => {
            url.searchParams.append(`rules[${rulesCounter}][rightSide]`, symbol.text)
        })
    })
}

