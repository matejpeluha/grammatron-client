import {AppConstants} from "../constants";
import {GrammarInputRule, GrammarInputSymbol} from "../interfaces/GrammarInput";

export const getEmptySymbol = (): GrammarInputSymbol => {
    return { text: "", error: 'errors.missing_symbol' }
}

export const getEpsilonSymbol = (): GrammarInputSymbol => {
    return { text: AppConstants.EPSILON, error: "" }
}

export const getEmptyRule = (): GrammarInputRule => {
    return {
        leftSide: getEmptySymbol(),
        rightSide: [ getEmptySymbol() ]
    }
}
