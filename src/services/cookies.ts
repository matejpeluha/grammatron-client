import {GrammarInput} from "../interfaces/GrammarInput";
import {useCookies} from "react-cookie";
import {useState} from "react";

const cookieOptions = {
    maxAge: 15000000,
    secure: true
}

export type CookieAction = 'save'|'load'|null|undefined

export const useGrammarCookies = () => {
    const [cookies, setCookie] = useCookies(['grammar', 'grammar-cookies-allowed'])
    const [isCookiesModalOpened, setIsCookiesModalOpened] = useState<boolean>(false)
    const [lastCookieAction, setLastCookieAction] = useState<CookieAction>('load')
    const areGrammarCookiesAllowed = cookies["grammar-cookies-allowed"] === 'true'

    const saveGrammar = (grammar: GrammarInput) => {
        if (areGrammarCookiesAllowed) {
            setCookie('grammar', grammar, cookieOptions)
        } else {
            setIsCookiesModalOpened(true)
        }
    }

    const changeCookiesSettings = (cookiesAllowance: boolean) => {
        setCookie('grammar-cookies-allowed', cookiesAllowance, cookieOptions)
        setIsCookiesModalOpened(false)
    }

    return {
        cookiesGrammar: areGrammarCookiesAllowed ? cookies.grammar as GrammarInput : null,
        saveGrammar,
        areCookiesAllowed: areGrammarCookiesAllowed,
        changeCookiesSettings,
        isCookiesModalOpened,
        openCookiesModal: () => setIsCookiesModalOpened(true),
        lastCookieAction,
        setLastCookieAction
    }
}
